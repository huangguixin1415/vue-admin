
const TokenKey = 'vue_mall_web_token'

export function getToken() {
  return JSON.parse(localStorage.getItem(TokenKey))
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, JSON.stringify(token))
}

export function removeToken() {
  return localStorage.clear()
}
