import request from '@/utils/request';

export function sysLogin(data) {
    return request({
        url: '/login/sys/',
        method: 'post',
        data
    });
}