import request from '@/utils/request';

export function page(targetPage = 1, rows = 10) {
    return request({
        url: '/user/sys/page',
        method: 'get',
        params: {
            targetPage,
            rows
        }
    });
}

export function remove(ids) {
    return request({
        url: '/user/sys/delete',
        method: 'delete',
        data: ids
    });
}

export function modify(userObj) {
    return request({
        url: '/user/sys/modify',
        method: 'put',
        data: userObj
    });
}

export function create(userObj) {
    return request({
        url: '/user/sys/create',
        method: 'post',
        data: userObj
    });
}