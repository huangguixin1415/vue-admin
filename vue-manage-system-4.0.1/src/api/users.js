import request from '@/utils/request';

export function login(data) {
    return request({
        url: '/users/login',
        method: 'post',
        data
    });
}

export function page(targetPage = 1, rows = 10) {
    return request({
        url: '/user/sys/page',
        method: 'get',
        params: {
            targetPage,
            rows
        }
    });
}

export function remove(ids) {
    return request({
        url: '/user/sys/delete',
        method: 'delete',
        data: ids
    });
}