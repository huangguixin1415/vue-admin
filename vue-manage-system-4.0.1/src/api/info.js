import request from '@/utils/request';

export function page(pageNo = 1, pageSize = 10,name='') {
    return request({
        url: '/info/page',
        method: 'get',
        params: {
            pageNo,
            pageSize,
            name
        }
    });
}

export function modify(data) {
    return request({
        url: '/info/modify',
        method: 'put',
        data
    });
}

export function create(data) {
    return request({
        url: '/info/create',
        method: 'post',
        data
    });
}

export function deletes(data) {
    return request({
        url: '/info/delete',
        method: 'delete',
        data
    });
}
