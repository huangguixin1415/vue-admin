import request from '@/utils/request';

export function page(pageNo = 1, pageSize = 10) {
    return request({
        url: '/monitor/page',
        method: 'get',
        params: {
            pageNo,
            pageSize
        }
    });
}

export function modify(data) {
    return request({
        url: '/monitor/modify',
        method: 'put',
        data
    });
}

export function create(data) {
    return request({
        url: '/monitor/create',
        method: 'post',
        data
    });
}

export function deletes(data) {
    return request({
        url: '/monitor/delete',
        method: 'delete',
        data
    });
}
