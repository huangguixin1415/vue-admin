import axios from 'axios';
import { Message } from 'element-ui';
import {baseURL} from '@/utils/config'

const service = axios.create({
    baseURL,
    timeout: 5000
});

service.interceptors.request.use(
    config => {
        return config;
    },
    error => {
        console.log(error);
        return Promise.reject();
    }
);

service.interceptors.response.use(
    response => {
        if (response.status === 200) {
            if (response.data.isError) {
                Message({
                    message: response.data.msg || 'Error',
                    type: 'error',
                    duration: 1000
                })
                return Promise.reject();
            } else {
                return response.data;
            }
        } else {
            return Promise.reject();
        }
    },
    error => {
        console.log(error);
        return Promise.reject();
    }
);

export default service;
